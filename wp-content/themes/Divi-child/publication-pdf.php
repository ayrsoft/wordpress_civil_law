<?php
/**
 * Template Name: Publication PDF
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 */



get_header();

$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() );

?>

<div id="main-content">

<?php if ( ! $is_page_builder_used ) : ?>

	<div class="container">
		<div id="content-area" class="clearfix">
			<div id="left-area">

<?php endif; ?>

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php if ( ! $is_page_builder_used ) : ?>

					<h1 class="entry-title main_title"><?php the_title(); ?></h1>
				<?php
					$thumb = '';

					$width = (int) apply_filters( 'et_pb_index_blog_image_width', 1080 );

					$height = (int) apply_filters( 'et_pb_index_blog_image_height', 675 );
					$classtext = 'et_featured_image';
					$titletext = get_the_title();
					$thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
					$thumb = $thumbnail["thumb"];

					if ( 'on' === et_get_option( 'divi_page_thumbnails', 'false' ) && '' !== $thumb )
						print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height );
				?>

				<?php endif; ?>

					<div class="entry-content">
					<?php
						the_content();

						if ( ! $is_page_builder_used )
							wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'Divi' ), 'after' => '</div>' ) );
					?>
					</div> <!-- .entry-content -->

				<?php
					if ( ! $is_page_builder_used && comments_open() && 'on' === et_get_option( 'divi_show_pagescomments', 'false' ) ) comments_template( '', true );
				?>

				</article> <!-- .et_pb_post -->

			<?php endwhile; ?>

<?php if ( ! $is_page_builder_used ) : ?>

			</div> <!-- #left-area -->

			<?php  get_sidebar(); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->

	

<?php endif; ?>



</div> <!-- #main-content -->

<!-- CUSTOM CONTENT START -->
<div class="container2">
	<ul class="row news-list">
	<?php 
			$paged = max(get_query_var('paged'),1);
			$args = array(
				'post_type' => 'pdf',
				'posts_per_page' => 8,
				'paged' => $paged,
				'order' => 'DESC'
			);

			$query = new WP_Query($args);

			while($query->have_posts()) :  
			$query->the_post();
    	?>
		<li class="col-3 col-m-6">
			<?php 
				$file = get_field('pdf_file');

				if( $file ): 
			?>
				<a href="<?php echo $file['url']; ?>" target="_blank">
					<?php the_post_thumbnail( 'large' , array( 'class'  => 'img-responsive') ); ?>
					<div class="desc">
						<h5><?php the_title(); ?></h5> 
						<span><?php the_field('sub_title'); ?></span>
						<span class="date"><?php the_time('F Y'); ?></span class="date">
					</div>
				</a>

			<?php endif; ?>
		</li>
	<?php 	
		endwhile;
		wp_reset_postdata();
	?>
	</ul>

	<div id="pagenav" class="text-center">
	<?php 
		if (function_exists('wp_pagenavi')) :
			wp_pagenavi( array('query' => $query) ); 
		endif; 
	 ?>

	</div>

</div>
<!-- CUSTOM CONTENT END -->
<!-- CUSTOM TEMPLATE START -->
<script type="text/javascript">

	(function($) {

	})( jQuery );

</script>
<!-- CUSTOM TEMPLATE END -->

<?php get_footer(); ?>
