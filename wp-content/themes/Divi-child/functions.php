<?php

function load_custom_child_js() {
	if (is_page('alumni-directory') || is_page('news-letter')) 
	{
		wp_enqueue_script('datatables-js', get_stylesheet_directory_uri() . '/jquery.dataTables.min.js','','1.1', true);
		wp_enqueue_script('custom-child-js', get_stylesheet_directory_uri() . '/custom-child.js','','1.1', true);
		wp_enqueue_style( 'datatables-css', get_stylesheet_directory_uri() . '/jquery.dataTables.min.css');
	}

	if(is_page('alumni-newsletter'))
	{
		wp_enqueue_style( 'publication-style-css', get_stylesheet_directory_uri() . '/publication-style.css');
	}
}
add_action( 'wp_enqueue_scripts', 'load_custom_child_js' );
