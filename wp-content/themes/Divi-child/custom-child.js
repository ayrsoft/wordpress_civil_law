(function($) {
	

	$('input[type="submit"]').click(function(){

		$('#myTable').dataTable().fnDestroy();

		var fname = $('#fname').val();
		var lname = $('#lname').val();
		var home_add = $('#home_add').val();
		var tel_num = $('#tel_num').val();
		var mobile_num = $('#mobile_num').val();
		var office_add = $('#office_add').val();
		var office_num = $('#office_num').val();
		var email = $('#email').val();
		var present_app = $('#present_app').val();
		var area_practice = $('#area_practice').val();
		var class_of = $('#class_of').val();

		console.log( " fname: " + fname + "lname: " + lname + "home add:  " + home_add + "tel num: " + tel_num + "mobile num: " + mobile_num +  " office add: " + office_add + "office num: " + office_num + "email: " + email + "present app:  " + present_app + "area practice:  " + area_practice + "class of: " + class_of);

		if(fname == "" || lname == "" || class_of == "" || area_practice == "" || email == "")
		{
			console.log('empty');
		}
		else
		{
			console.log("working");
			$.ajax({
		      url: '/submission/newsletter.php',
		      type: 'post',
		      data: {
		      	'action': 'sign_up', 
		      	'fname': fname,
		      	'lname': lname,
		      	'home_add': home_add,
		      	'tel_num': tel_num,
		      	'mobile_num': mobile_num,
		      	'office_add': office_add,
		      	'office_num': office_num,
		      	'email': email,
		      	'present_app': present_app,
		      	'area_practice': area_practice,
		      	'class_of': class_of

		      },
		      success: function(data, status) {
		        if(data) {
		         console.log(data);
				
				setInterval('location.reload()', 2000);

			

		        }
		      },
		      error: function(xhr, desc, err) {
		        console.log(xhr);
		        console.log("Details: " + desc + "\nError:" + err);
		      }
		    }); 
		}
	});

    $('#alumni-subscribers').DataTable();
    
})( jQuery );