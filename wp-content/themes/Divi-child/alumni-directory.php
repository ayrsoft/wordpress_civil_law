<?php
/**
 * Template Name: Alumni Directory
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package selectedgroup
 */



get_header();

$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() );

?>

<div id="main-content">

<?php if ( ! $is_page_builder_used ) : ?>

	<div class="container">
		<div id="content-area" class="clearfix">
			<div id="left-area">

<?php endif; ?>

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php if ( ! $is_page_builder_used ) : ?>

					<h1 class="entry-title main_title"><?php the_title(); ?></h1>
				<?php
					$thumb = '';

					$width = (int) apply_filters( 'et_pb_index_blog_image_width', 1080 );

					$height = (int) apply_filters( 'et_pb_index_blog_image_height', 675 );
					$classtext = 'et_featured_image';
					$titletext = get_the_title();
					$thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
					$thumb = $thumbnail["thumb"];

					if ( 'on' === et_get_option( 'divi_page_thumbnails', 'false' ) && '' !== $thumb )
						print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height );
				?>

				<?php endif; ?>

					<div class="entry-content">
					<?php
						the_content();

						if ( ! $is_page_builder_used )
							wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'Divi' ), 'after' => '</div>' ) );
					?>
					</div> <!-- .entry-content -->

				<?php
					if ( ! $is_page_builder_used && comments_open() && 'on' === et_get_option( 'divi_show_pagescomments', 'false' ) ) comments_template( '', true );
				?>

				</article> <!-- .et_pb_post -->

			<?php endwhile; ?>

<?php if ( ! $is_page_builder_used ) : ?>

			</div> <!-- #left-area -->

			<?php  get_sidebar(); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->

<?php endif; ?>


<!-- CUSTOM CONTENT START -->		
<div class=" et_pb_row et_pb_row_1" id="table-style-alumni">
	<div class="et_pb_column et_pb_column_4_4  et_pb_column_1">
				
	<table id="alumni-subscribers" class="display" cellspacing="0" width="100%">
	<thead>
        <tr>
            <th>Firstname</th>
            <th>Lastname</th>
            <th>Class of</th>
            <th>Present Application</th>
        </tr>
    </thead>
    <tbody>
<?php 
global $wpdb;
global $post;
$results = $wpdb->get_results("select * from wp_subscribers");
if($results)
{
	foreach ($results as $row) {
		echo "<tr>";
		echo "<td>".$row->firstname."</td>";
		echo "<td>".$row->lastname."</td>";
		echo "<td>".$row->class_of."</td>";
		echo "<td>".$row->present_application."</td>";
		echo "</tr>";
	}
}
else
{
	echo "<h4>No Alumnus Subscribers</h4>";
}
?>
		</tbody>
	</table>
</div> <!-- .et_pb_column -->
		
</div> <!-- .et_pb_row -->
	
<!-- CUSTOM CONTENT END -->

</div> <!-- #main-content -->

<?php get_footer(); ?>
