<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */

$whitelist = array(
    '127.0.0.1',
    '::1'
);

if(!in_array($_SERVER['REMOTE_ADDR'], $whitelist)){
	/** MySQL database username */
	define('DB_USER', 'civillawuser');
	/** MySQL database password */
	define('DB_PASSWORD', '2c9075a7911b5ed78d36e9b373ac3d5a4d31056d6767a9cc');
}else{
	/** MySQL database username */
	define('DB_USER', 'root');
	/** MySQL database password */
	define('DB_PASSWORD', 'root');
}

define('DB_NAME', 'db_civillaw');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '`+]xD Y;^m1t ^Y_u$w,Hp+csx1pQX87g+:p5 ^hgr<{:q:I~Zii{>]?wi2@u~ut');
define('SECURE_AUTH_KEY',  'EVG(-^2XV/((-#e3rm@>+[~SB.|3 @&+|Bw;<yF1l|p?2#|f1eLqb]%{Wx 7z?eb');
define('LOGGED_IN_KEY',    'o`b$:3`]6cS,n!$RVk#5# %$bi~AP+(DkPD>qA]Z7O%(FN>qlc GK&;tRWlyzVWj');
define('NONCE_KEY',        '<STHW*7 }plA_I)`n!nQ.@({yL)L[Fi,t7kR%$qj0AA+jx4~[Fn(v4VPnn)a2j9w');
define('AUTH_SALT',        '~NVg?,xr;hS}8E2+u-X_]);a:@Vz9t1ID;[d6(9iTI+10k~^~rG%hL&jYI9i>7Y4');
define('SECURE_AUTH_SALT', '0u67|.5-<*Ef.JmhtJ$?Br|?KP|r8weRV+!t5?d7m4`EV>=F;i^YP~mO5B[=91?u');
define('LOGGED_IN_SALT',   'IN+!>?=gqrd1|( rQ-_k@Y$SPiN@| O+|FytiB:;[I!ZLFwk}!E^40b (T-{Nos5');
define('NONCE_SALT',       '>69{9wfP+FWvv+fqW!e%{G5O0+]P6nN}~mS$uwlsa7*ZK6mQ^e|v5ig8_$?e*>7m');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);
define('FS_METHOD', 'direct');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
